package com.jsmile.cloud.goodscenter.spu.event;

import com.jsmile.cloud.goodscenter.spu.model.SkuInfo;

import lombok.Getter;

@Getter
public class SkuDeleteEvent extends SkuEvent {

    public SkuDeleteEvent(SkuInfo skuInfo) {
        super(skuInfo);
    }
}
