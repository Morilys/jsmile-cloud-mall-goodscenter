package com.jsmile.cloud.goodscenter.web.mq.order;

import com.jsmile.cloud.tradecenter.api.enums.OrderStatusEnum;
import com.jsmile.cloud.tradecenter.api.to.OrderStatusChangeTo;

public interface IOrderStatusStrategy {

    /**
     * 订单状态
     *
     * @return
     */
    OrderStatusEnum orderStatus();

    /**
     * 事件处理
     *
     * @param orderStatusChangeTo
     *            订单信息
     */
    void execute(OrderStatusChangeTo orderStatusChangeTo);
}
