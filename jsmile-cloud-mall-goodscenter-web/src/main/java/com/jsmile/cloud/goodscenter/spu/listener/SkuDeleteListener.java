package com.jsmile.cloud.goodscenter.spu.listener;

import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.jsmile.cloud.goodscenter.spu.event.SkuDeleteEvent;

import lombok.extern.slf4j.Slf4j;

/**
 * 商品sku删除后通知各活动下架
 */
@Slf4j
@Component
public class SkuDeleteListener {

    @Async
    @Order
    @EventListener(SkuDeleteEvent.class)
    public void orderStatusChange(SkuDeleteEvent event) {
        log.info("sku删除：{}", event.getSkuInfo());
    }

}
