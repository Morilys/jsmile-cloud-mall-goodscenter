package com.jsmile.cloud.goodscenter.spu.repo;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jsmile.cloud.goodscenter.spu.dao.SkuSpecDao;
import com.jsmile.cloud.goodscenter.spu.model.SkuSpec;

import lombok.extern.slf4j.Slf4j;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2020年06月04日 03:56:49
 */
@Service
@Slf4j
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class SkuSpecSearchRepository extends ServiceImpl<SkuSpecDao, SkuSpec> {

    public List<SkuSpec> findBySkuIdAndSpuId(Long skuId, Long spuId) {
        return this.baseMapper.findBySkuIdAndSpuId(skuId, spuId);
    }

    public List<SkuSpec> findBySpuId(Long spuId) {
        return this.baseMapper.findBySpuId(spuId);
    }
}
