package com.jsmile.cloud.goodscenter.spu.feign;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jsmile.cloud.goodscenter.api.req.spu.ReqSkuCheck;
import com.jsmile.cloud.goodscenter.api.res.ResSku;
import com.jsmile.cloud.goodscenter.spu.service.SkuApplicationService;
import com.jsmile.mail.security.controller.AuthcController;
import com.jsmile.mail.security.handler.JsmileContext;
import com.jsmile.mall.api.JSmileResult;
import com.jsmile.mall.api.annotation.CurrentContext;
import com.jsmile.mall.api.constants.JSmileConstant;
import com.jsmile.mall.api.enums.LogTypeEnum;
import com.jsmile.mall.log.annotation.JsmileLog;

import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@AllArgsConstructor
@Api(tags = "sku管理")
public class SkuServiceApiImpl extends AuthcController {

    private final SkuApplicationService skuApplicationService;

    @RequestMapping(value = "/feign/sku/skuCheck", method = RequestMethod.POST)
    @JsmileLog(title = "商品中心->商品库存和状态检查", type = LogTypeEnum.QUERY, systemId = JSmileConstant.SystemId.GOODS_CENTER)
    public JSmileResult skuCheck(@RequestBody @Valid ReqSkuCheck reqSkuCheck, @CurrentContext JsmileContext jsmileContext) {
        this.initContext(jsmileContext, JSmileConstant.SystemId.GOODS_CENTER);
        reqSkuCheck.setCurTenantId(jsmileContext.getTenantId());
        List<ResSku> skuList = skuApplicationService.skuCheck(reqSkuCheck);
        return JSmileResult.success(skuList);
    }

}
