package com.jsmile.cloud.goodscenter.spu.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.jsmile.cloud.goodscenter.spu.model.RepertoryRecord;
import com.jsmile.cloud.goodscenter.spu.repo.RepertoryRecordRepository;
import com.jsmile.cloud.goodscenter.spu.repo.SpuRepository;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@AllArgsConstructor
public class RepertoryRecordApplicationService {

    private final SpuRepository spuRepository;
    private final RepertoryRecordRepository repertoryRecordRepository;

    public void create(List<RepertoryRecord> records) {
        log.info("库存扣减记录:{]", records);
        records.forEach(x -> {
            if (0 == x.getStatus()) {
                spuRepository.reduceStock(x.getSpuId(), x.getSkuId(), x.getNum());
            } else {
                spuRepository.addStock(x.getSpuId(), x.getSkuId(), x.getNum());
            }
        });
        repertoryRecordRepository.saveBatch(records);
    }

}
