package com.jsmile.cloud.goodscenter.spu.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jsmile.cloud.goodscenter.spu.model.SkuInfo;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2020年06月04日 03:56:49
 */
public interface SkuInfoDao extends BaseMapper<SkuInfo> {

    @Select("select * from t_sku_info where spu_id=#{spuId} AND tenant_id=#{tenantId}")
    List<SkuInfo> findBySpuIdAndTenantId(@Param("spuId") Long spuId, @Param("tenantId") Long tenantId);

    @Delete("delete from t_sku_info where  spu_id=#{spuId} AND tenant_id=#{tenantId}")
    int deleteBySpuIdAndTenantId(@Param("spuId") Long spuId, @Param("tenantId") Long tenantId);

    @Update("update  t_sku_info set inventory=inventory-#{skuNumber} where spu_id=#{spuId} and sku_id=#{skuId} ")
    int reduceStock(@Param("spuId") Long spuId, @Param("skuId") Long skuId, @Param("skuNumber") int skuNumber);

    @Update("update  t_sku_info set inventory=inventory+#{skuNumber} where spu_id=#{spuId} and sku_id=#{skuId} ")
    int addStock(@Param("spuId") Long spuId, @Param("skuId") Long skuId, @Param("skuNumber") int skuNumber);
}
