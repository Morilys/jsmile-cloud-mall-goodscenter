package com.jsmile.cloud.goodscenter.spu.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2020年06月04日 03:56:49
 */
@ApiModel
@Data
@TableName("t_repertory_record")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RepertoryRecord extends Model<RepertoryRecord> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @ApiModelProperty(value = "订单编号")
    private String orderNo;
    @ApiModelProperty(value = "活动id")
    private Long activityId;
    @ApiModelProperty(value = "skuid")
    private Long skuId;
    @ApiModelProperty(value = "skuid")
    private Long spuId;
    @ApiModelProperty(value = "店铺id")
    private Long shopId;
    @ApiModelProperty(value = "扣减数量")
    private Integer num;
    @ApiModelProperty(value = "扣减时间")
    private java.util.Date createTime;
    @ApiModelProperty(value = "回滚时间")
    private java.util.Date updateTime;
    @ApiModelProperty(value = "0:扣减 1:回退")
    private Integer status;
    @ApiModelProperty(value = "租户id")
    private Long tenantId;
    @ApiModelProperty(value = "平台id")
    private Integer platformId;
    @ApiModelProperty(value = "业务id")
    private Integer bizCode;

}
