package com.jsmile.cloud.goodscenter.cate.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2020年06月04日 04:18:34
 */
@ApiModel
@Data
@TableName("t_front_end_cate")
public class FrontEndCate extends Model<FrontEndCate> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @ApiModelProperty(value = "前端分类id")
    private Long frontCateId;
    @ApiModelProperty(value = "后端分类id")
    private Long categoryId;
    @ApiModelProperty(value = "租户id")
    private Long tenantId;
    @ApiModelProperty(value = "删除标识")
    private Integer delFlag;
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
    @ApiModelProperty(value = "平台id")
    private Integer platformId;
    @ApiModelProperty(value = "业务id")
    private Integer bizCode;

}
