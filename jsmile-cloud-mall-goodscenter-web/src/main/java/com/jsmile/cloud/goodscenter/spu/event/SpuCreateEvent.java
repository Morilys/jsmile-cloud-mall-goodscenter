package com.jsmile.cloud.goodscenter.spu.event;

import com.jsmile.cloud.goodscenter.spu.model.Spu;

import lombok.Getter;

@Getter
public class SpuCreateEvent extends SpuEvent {

    public SpuCreateEvent(Spu spu) {
        super(spu);
    }
}
