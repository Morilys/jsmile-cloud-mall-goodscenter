package com.jsmile.cloud.goodscenter.web.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.alibaba.druid.filter.Filter;
import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.wall.WallConfig;
import com.alibaba.druid.wall.WallFilter;

@EnableTransactionManagement
@Configuration
@Primary
public class DataSourceConfig {

    /**
     * druid数据库连接池
     */
    @Primary
    @ConfigurationProperties(prefix = "goodscenter.datasource")
    @Bean(name = "datasource")
    public DruidDataSource dataSource() {
        DruidDataSource druidDataSource = new DruidDataSource();
        List<Filter> filterList = new ArrayList<>();
        filterList.add(wallFilter());
        druidDataSource.setProxyFilters(filterList);
        return druidDataSource;
    }

    // @Bean
    // public PlatformTransactionManager txManager() {
    // DataSourceTransactionManager transactionManager
    // = new DataSourceTransactionManager(dataSource());
    // return new ChainedTransactionManager(transactionManager);
    // }
    //
    // @Override
    // public PlatformTransactionManager annotationDrivenTransactionManager() {
    // return txManager();
    // }

    @Bean
    public WallFilter wallFilter() {
        WallFilter wallFilter = new WallFilter();
        wallFilter.setConfig(wallConfig());
        return wallFilter;
    }

    @Bean
    public WallConfig wallConfig() {
        WallConfig config = new WallConfig();
        config.setMultiStatementAllow(true);// 允许一次执行多条语句
        config.setNoneBaseStatementAllow(true);// 允许非基本语句的其他语句
        return config;
    }

}
