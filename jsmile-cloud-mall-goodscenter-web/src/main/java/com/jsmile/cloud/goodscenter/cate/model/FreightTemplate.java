package com.jsmile.cloud.goodscenter.cate.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2020年06月04日 04:18:34
 */
@ApiModel
@Data
@TableName("t_freight_template")
public class FreightTemplate extends Model<FreightTemplate> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @ApiModelProperty(value = "模板id")
    private Long templateId;
    @ApiModelProperty(value = "租户id")
    private Long tenantId;
    @ApiModelProperty(value = "分组id")
    private Long groupId;
    @ApiModelProperty(value = "运费")
    private Long freight;
    @ApiModelProperty(value = "模板名称")
    private String name;
    @ApiModelProperty(value = "行政区号")
    private Integer areacode;
    @ApiModelProperty(value = "行政区名称")
    private String areaname;
    @ApiModelProperty(value = "满多少包邮")
    private Long freeMin;
    @ApiModelProperty(value = "层级，0：全国，1：省，2：市，3：区")
    private Boolean level;
    @ApiModelProperty(value = "extend")
    private String extend;
    @ApiModelProperty(value = "del_flag")
    private Integer delFlag;
    @ApiModelProperty(value = "create_time")
    private java.util.Date createTime;
    @ApiModelProperty(value = "update_time")
    private java.util.Date updateTime;
    @ApiModelProperty(value = "创建者")
    private String createBy;
    @ApiModelProperty(value = "修改者")
    private String updateBy;
    @ApiModelProperty(value = "平台id")
    private Integer platformId;
    @ApiModelProperty(value = "业务id")
    private Integer bizCode;

}
