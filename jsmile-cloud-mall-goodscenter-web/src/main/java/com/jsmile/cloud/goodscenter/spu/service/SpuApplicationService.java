package com.jsmile.cloud.goodscenter.spu.service;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jsmile.cloud.goodscenter.api.req.spu.*;
import com.jsmile.cloud.goodscenter.api.res.ResSpu;
import com.jsmile.cloud.goodscenter.spu.model.SkuSpec;
import com.jsmile.cloud.goodscenter.spu.model.Spu;
import com.jsmile.cloud.goodscenter.spu.repo.SkuInfoRepository;
import com.jsmile.cloud.goodscenter.spu.repo.SkuSpecSearchRepository;
import com.jsmile.cloud.goodscenter.spu.repo.SpuRepository;
import com.jsmile.cloud.goodscenter.spu.repo.SpuSearchRepository;
import com.jsmile.mall.api.enums.JSmileCacheEnum;

import cn.hutool.core.bean.BeanUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@AllArgsConstructor
public class SpuApplicationService {

    private final SpuRepository spuRepository;
    private final SkuInfoRepository skuInfoRepository;
    private final SpuSearchRepository spuSearchRepository;
    private final SkuSpecSearchRepository skuSpecSearchRepository;

    public ResSpu getSpu(ReqSpuDetail reqSpuDetail) {
        log.info("单商品详情:{]", reqSpuDetail);
        Spu spu = spuSearchRepository.getSpu(reqSpuDetail.getSpuId(), reqSpuDetail.getCurTenantId());
        ResSpu resSpu = BeanUtil.toBean(spu, ResSpu.class);
        List<SkuSpec> specs = skuSpecSearchRepository.findBySpuId(spu.getSpuId());
        resSpu.setSpecs(SkuSpec.getResSpecs(specs));
        return resSpu;
    }

    public ResSpu getSimpleSpu(ReqSpuDetail reqSpuDetail) {
        log.info("单商品详情:{]", reqSpuDetail);
        Spu spu = spuSearchRepository.getSimpleSpu(reqSpuDetail.getSpuId(), reqSpuDetail.getCurTenantId());
        return BeanUtil.toBean(spu, ResSpu.class);
    }

    public ResSpu addSpu(ReqSpuAdd reqSpuAdd) {
        log.info("商品新增:{]", reqSpuAdd);
        Spu spu = Spu.create(reqSpuAdd);
        spuRepository.saveSpu(spu);
        return BeanUtil.toBean(spu, ResSpu.class);
    }

    public ResSpu editSpu(ReqSpuEdit reqSpuEdit) {
        log.info("商品编辑:{]", reqSpuEdit);
        Spu spu = Spu.edit(reqSpuEdit);
        spuRepository.editSpu(spu);
        return BeanUtil.toBean(spu, ResSpu.class);
    }

    @CacheEvict(value = JSmileCacheEnum.JSmileCacheName.oneMonth, key = "'spu_'+#p0.spuId+'tenantId'+#p0.tenantId")
    public ResSpu putSpuByMerchant(ReqSpuSaleStatus reqSpuSaleStatus) {
        log.info("商户商品上架:{]", reqSpuSaleStatus);
        Spu spu = spuSearchRepository.getSpu(reqSpuSaleStatus.getSpuId(), reqSpuSaleStatus.getCurTenantId());
        spu.putSpuByMerchant(reqSpuSaleStatus);
        spuRepository.editSpuBySku(spu);
        return BeanUtil.toBean(spu, ResSpu.class);
    }

    @CacheEvict(value = JSmileCacheEnum.JSmileCacheName.oneMonth, key = "'spu_'+#p0.spuId+'tenantId'+#p0.tenantId")
    public ResSpu pullSpuByMerchant(ReqSpuDelete reqSpuDelete) {
        log.info("商户商品下架:{]", reqSpuDelete);
        Spu spu = spuSearchRepository.getSpu(reqSpuDelete.getSpuId(), reqSpuDelete.getCurTenantId());
        spu.pullSpuByMerchant();
        spuRepository.updateById(spu);
        return BeanUtil.toBean(spu, ResSpu.class);
    }

    @CacheEvict(value = JSmileCacheEnum.JSmileCacheName.oneMonth, key = "'spu_'+#p0.spuId+'tenantId'+#p0.tenantId")
    public ResSpu reviewSpu(ReqSpuReview reqSpuReview) {
        log.info("商品审核通过并发布:{]", reqSpuReview);
        Spu spu = spuSearchRepository.getSpu(reqSpuReview.getSpuId(), reqSpuReview.getCurTenantId());
        spu.reviewSpu(reqSpuReview);
        spuRepository.editSpuBySku(spu);
        return BeanUtil.toBean(spu, ResSpu.class);
    }

    @CacheEvict(value = JSmileCacheEnum.JSmileCacheName.oneMonth, key = "'spu_'+#p0.spuId+'tenantId'+#p0.tenantId")
    public ResSpu reviewSpuNot(ReqSpuReviewNot reqSpuReviewNot) {
        log.info("商品审核不通过:{]", reqSpuReviewNot);
        Spu spu = spuSearchRepository.getSpu(reqSpuReviewNot.getSpuId(), reqSpuReviewNot.getCurTenantId());
        spu.reviewSpuNot(reqSpuReviewNot);
        spuRepository.updateById(spu);
        return BeanUtil.toBean(spu, ResSpu.class);
    }

    public IPage<ResSpu> getSpuPageList(ReqSpuQuery reqSpuQuery) {
        log.info("供应商、运营、商户(小程序平台)商品查询:{}", reqSpuQuery);
        return spuSearchRepository.pageQuery(reqSpuQuery).convert(x -> {
            return BeanUtil.toBean(x, ResSpu.class);
        });
    }

}
