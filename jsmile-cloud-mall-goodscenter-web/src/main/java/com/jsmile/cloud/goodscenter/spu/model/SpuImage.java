package com.jsmile.cloud.goodscenter.spu.model;

import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.google.common.collect.Lists;
import com.jsmile.cloud.goodscenter.api.enums.SpuImageTypeEnum;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2020年06月04日 03:56:49
 */
@ApiModel
@Data
@TableName("t_spu_image")
public class SpuImage extends Model<SpuImage> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @ApiModelProperty(value = "spu_id")
    private Long spuId;
    @ApiModelProperty(value = "图片类型 1:头图 2:详情")
    private Integer type;
    @ApiModelProperty(value = "排序")
    private Integer sort;
    @ApiModelProperty(value = "图片url")
    private String imageUrl;
    @ApiModelProperty(value = "租户id")
    private Long tenantId;
    @ApiModelProperty(value = "平台id")
    private Integer platformId;
    @ApiModelProperty(value = "业务id")
    private Integer bizCode;

    public static List<SpuImage> createHeadImage(List<String> imageUrl, Long tenantId, Long spuId) {
        List<SpuImage> spuImages = Lists.newArrayList();
        imageUrl.forEach(x -> {
            SpuImage spuImage = new SpuImage();
            spuImage.setSpuId(spuId);
            spuImage.setTenantId(tenantId);
            spuImage.setImageUrl(x);
            spuImage.setType(SpuImageTypeEnum.HEAD.getValue());
            spuImages.add(spuImage);
        });
        return spuImages;
    }

    public static List<SpuImage> createDetailImage(List<String> imageDetailUrl, Long tenantId, Long spuId) {
        List<SpuImage> spuImages = Lists.newArrayList();
        imageDetailUrl.forEach(x -> {
            SpuImage spuImage = new SpuImage();
            spuImage.setSpuId(spuId);
            spuImage.setTenantId(tenantId);
            spuImage.setImageUrl(x);
            spuImage.setType(SpuImageTypeEnum.DETAIL.getValue());
            spuImages.add(spuImage);
        });
        return spuImages;
    }
}
