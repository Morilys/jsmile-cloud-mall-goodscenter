package com.jsmile.cloud.goodscenter.spu.event;

import com.jsmile.cloud.goodscenter.spu.model.Spu;

import lombok.Getter;

/**
 * 商品编辑事件
 */
@Getter
public class SpuEditEvent extends SpuEvent {

    public SpuEditEvent(Spu spu) {
        super(spu);
    }
}
