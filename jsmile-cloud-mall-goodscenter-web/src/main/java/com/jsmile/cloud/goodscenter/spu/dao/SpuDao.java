package com.jsmile.cloud.goodscenter.spu.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jsmile.cloud.goodscenter.spu.model.Spu;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2020年06月04日 03:56:49
 */
public interface SpuDao extends BaseMapper<Spu> {

    @Select("select * from t_spu where spu_id=#{spuId} AND tenant_id=#{tenantId} limit 1")
    public Spu findOneBySpuIdAndTenantId(@Param("spuId") Long spuId, @Param("tenantId") Long tenantId);

    @Update("update  t_spu set goods_stock=goods_stock-#{skuNumber},goods_sales=goods_sales+#{skuNumber} where spu_id=#{spuId} ")
    public int reduceStock(@Param("spuId") Long spuId, @Param("skuNumber") int skuNumber);

    @Update("update  t_spu set goods_stock=goods_stock+#{skuNumber},goods_sales=goods_sales-#{skuNumber} where spu_id=#{spuId} ")
    public int addStock(@Param("spuId") Long spuId, @Param("skuNumber") int skuNumber);
}
