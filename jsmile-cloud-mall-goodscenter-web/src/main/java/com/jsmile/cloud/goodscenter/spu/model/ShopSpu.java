package com.jsmile.cloud.goodscenter.spu.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.jsmile.cloud.goodscenter.api.enums.GoodsCodeEnum;
import com.jsmile.cloud.goodscenter.api.enums.SaleStatusEnum;
import com.jsmile.mall.api.exception.ExceptionKit;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2020年06月04日 03:56:49
 */
@ApiModel
@Data
@TableName("t_shop_spu")
public class ShopSpu extends Model<ShopSpu> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @ApiModelProperty(value = "商品id")
    private Long spuId;
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
    @ApiModelProperty(value = "创建者")
    private String createBy;
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
    @ApiModelProperty(value = "更新者")
    private String updateBy;
    @ApiModelProperty(value = "删除标记位 0:有效 1:删除")
    @TableLogic
    private Integer delFlag;
    @ApiModelProperty(value = "店铺上架状态 1:上架 0:下架")
    private Integer saleStatus;
    @ApiModelProperty(value = "商户id")
    private Long tenantId;
    @ApiModelProperty(value = "店铺id")
    private Long shopId;
    @ApiModelProperty(value = "平台id")
    private Integer platformId;
    @ApiModelProperty(value = "业务id")
    private Integer bizCode;

    public void checkStatus() {
        ExceptionKit.checkArgument(SaleStatusEnum.PUT.getValue() != this.getSaleStatus(), GoodsCodeEnum.GOODS_DOWN.getResCode(), GoodsCodeEnum.GOODS_DOWN.getResMsg());
    }
}
