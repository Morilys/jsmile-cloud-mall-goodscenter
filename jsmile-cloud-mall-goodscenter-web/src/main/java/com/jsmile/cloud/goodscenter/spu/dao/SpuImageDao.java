package com.jsmile.cloud.goodscenter.spu.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jsmile.cloud.goodscenter.spu.model.SpuImage;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2020年06月04日 03:56:49
 */
public interface SpuImageDao extends BaseMapper<SpuImage> {

    @Delete("delete from t_spu_image where  spu_id=#{spuId} ")
    public int deleteBySpuId(@Param("spuId") Long spuId);

    @Select("select * from t_spu_image where spu_id=#{spuId}  AND type=#{type}")
    public List<SpuImage> findBySpuIdAndType(@Param("spuId") Long spuId, @Param("type") Integer type);
}
