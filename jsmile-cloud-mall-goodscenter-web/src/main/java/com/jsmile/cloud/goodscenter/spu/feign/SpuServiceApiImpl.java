package com.jsmile.cloud.goodscenter.spu.feign;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jsmile.cloud.goodscenter.api.req.spu.*;
import com.jsmile.cloud.goodscenter.api.res.ResSpu;
import com.jsmile.cloud.goodscenter.spu.service.SpuApplicationService;
import com.jsmile.mail.security.controller.AuthcController;
import com.jsmile.mail.security.handler.JsmileContext;
import com.jsmile.mall.api.JSmileResult;
import com.jsmile.mall.api.annotation.CurrentContext;
import com.jsmile.mall.api.constants.JSmileConstant;
import com.jsmile.mall.api.enums.LogTypeEnum;
import com.jsmile.mall.api.to.JsmilePage;
import com.jsmile.mall.log.annotation.JsmileLog;

import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@AllArgsConstructor
@Api(tags = "spu管理")
public class SpuServiceApiImpl extends AuthcController {

    private final SpuApplicationService spuApplicationService;

    @RequestMapping(value = "/feign/spu/addSpu", method = RequestMethod.POST)
    @JsmileLog(title = "商品中心->新增商品", type = LogTypeEnum.INSERT, systemId = JSmileConstant.SystemId.GOODS_CENTER)
    public JSmileResult addSpu(@RequestBody @Valid ReqSpuAdd reqSpuAdd, @CurrentContext JsmileContext jsmileContext) {
        this.initContext(jsmileContext, JSmileConstant.SystemId.GOODS_CENTER);
        reqSpuAdd.setCurTenantId(jsmileContext.getTenantId());
        reqSpuAdd.setSupplyId(jsmileContext.getSupplyId());
        reqSpuAdd.setShopId(jsmileContext.getShopId());
        ResSpu spu = spuApplicationService.addSpu(reqSpuAdd);
        return JSmileResult.success(spu);
    }

    @RequestMapping(value = "/feign/spu/editSpu", method = RequestMethod.POST)
    @JsmileLog(title = "商品中心->编辑商品", type = LogTypeEnum.UPDATE, systemId = JSmileConstant.SystemId.GOODS_CENTER)
    public JSmileResult editSpu(@RequestBody @Valid ReqSpuEdit reqSpuEdit, @CurrentContext JsmileContext jsmileContext) {
        this.initContext(jsmileContext, JSmileConstant.SystemId.GOODS_CENTER);
        reqSpuEdit.setCurTenantId(jsmileContext.getTenantId());
        ResSpu spu = spuApplicationService.editSpu(reqSpuEdit);
        return JSmileResult.success(spu);
    }

    @RequestMapping(value = "/feign/spu/putSpuByMerchant", method = RequestMethod.POST)
    @JsmileLog(title = "商品中心->商品上架", type = LogTypeEnum.UPDATE, systemId = JSmileConstant.SystemId.GOODS_CENTER)
    public JSmileResult putSpuByMerchant(@RequestBody @Valid ReqSpuSaleStatus reqSpuSaleStatus, @CurrentContext JsmileContext jsmileContext) {
        this.initContext(jsmileContext, JSmileConstant.SystemId.GOODS_CENTER);
        reqSpuSaleStatus.setCurTenantId(jsmileContext.getTenantId());
        ResSpu spu = spuApplicationService.putSpuByMerchant(reqSpuSaleStatus);
        return JSmileResult.success(spu);
    }

    @RequestMapping(value = "/feign/spu/pullSpuByMerchant", method = RequestMethod.POST)
    @JsmileLog(title = "商品中心->商品下架", type = LogTypeEnum.UPDATE, systemId = JSmileConstant.SystemId.GOODS_CENTER)
    public JSmileResult pullSpuByMerchant(@RequestBody @Valid ReqSpuDelete reqSpuDelete, @CurrentContext JsmileContext jsmileContext) {
        this.initContext(jsmileContext, JSmileConstant.SystemId.GOODS_CENTER);
        reqSpuDelete.setCurTenantId(jsmileContext.getTenantId());
        ResSpu spu = spuApplicationService.pullSpuByMerchant(reqSpuDelete);
        return JSmileResult.success(spu);
    }

    @RequestMapping(value = "/feign/spu/reviewSpu", method = RequestMethod.POST)
    @JsmileLog(title = "商品中心->商品审核通过并发布", type = LogTypeEnum.UPDATE, systemId = JSmileConstant.SystemId.GOODS_CENTER)
    public JSmileResult reviewSpu(@RequestBody @Valid ReqSpuReview reqSpuReview, @CurrentContext JsmileContext jsmileContext) {
        this.initContext(jsmileContext, JSmileConstant.SystemId.GOODS_CENTER);
        reqSpuReview.setCurTenantId(jsmileContext.getTenantId());
        ResSpu spu = spuApplicationService.reviewSpu(reqSpuReview);
        return JSmileResult.success(spu);
    }

    @RequestMapping(value = "/feign/spu/reviewSpuNot", method = RequestMethod.POST)
    @JsmileLog(title = "商品中心->商品审核不通过", type = LogTypeEnum.UPDATE, systemId = JSmileConstant.SystemId.GOODS_CENTER)
    public JSmileResult reviewSpuNot(@RequestBody @Valid ReqSpuReviewNot reqSpuReviewNot, @CurrentContext JsmileContext jsmileContext) {
        this.initContext(jsmileContext, JSmileConstant.SystemId.GOODS_CENTER);
        reqSpuReviewNot.setCurTenantId(jsmileContext.getTenantId());
        ResSpu spu = spuApplicationService.reviewSpuNot(reqSpuReviewNot);
        return JSmileResult.success(spu);
    }

    @RequestMapping(value = "/feign/spu/getSpu", method = RequestMethod.POST)
    public JSmileResult getSpu(@RequestBody @Valid ReqSpuDetail reqSpuDetail, @CurrentContext JsmileContext jsmileContext) {
        this.initContext(jsmileContext, JSmileConstant.SystemId.GOODS_CENTER);
        reqSpuDetail.setCurTenantId(jsmileContext.getTenantId());
        ResSpu spu = spuApplicationService.getSpu(reqSpuDetail);
        return JSmileResult.success(spu);
    }

    @RequestMapping(value = "/feign/spu/getSimpleSpu", method = RequestMethod.POST)
    public JSmileResult getSimpleSpu(@RequestBody @Valid ReqSpuDetail reqSpuDetail, @CurrentContext JsmileContext jsmileContext) {
        this.initContext(jsmileContext, JSmileConstant.SystemId.GOODS_CENTER);
        reqSpuDetail.setCurTenantId(jsmileContext.getTenantId());
        ResSpu spu = spuApplicationService.getSimpleSpu(reqSpuDetail);
        return JSmileResult.success(spu);
    }

    @RequestMapping(value = "/feign/spu/getSpuPageList", method = RequestMethod.POST)
    public JSmileResult<JsmilePage<ResSpu>> getSpuPageList(@RequestBody @Valid ReqSpuQuery reqSpuQuery, @CurrentContext JsmileContext jsmileContext) {
        this.initContext(jsmileContext, JSmileConstant.SystemId.GOODS_CENTER);
        reqSpuQuery.setCurTenantId(jsmileContext.getTenantId());
        IPage<ResSpu> pages = spuApplicationService.getSpuPageList(reqSpuQuery);
        return JSmileResult.success(new JsmilePage(reqSpuQuery.getPageIndex(), reqSpuQuery.getPageSize(), pages.getTotal(), pages.getRecords()));
    }
}
