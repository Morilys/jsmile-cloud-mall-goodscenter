package com.jsmile.cloud.goodscenter.spu.event;

import com.jsmile.cloud.goodscenter.spu.model.Spu;

import lombok.Getter;

/**
 * 商品停售事件
 */
@Getter
public class SpuStopEvent extends SpuEvent {

    public SpuStopEvent(Spu spu) {
        super(spu);
    }
}
