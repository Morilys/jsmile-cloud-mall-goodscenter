package com.jsmile.cloud.goodscenter.web.mq;

import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.jsmile.cloud.goodscenter.web.mq.order.OrderStatusChangeStrategyMap;
import com.jsmile.cloud.tradecenter.api.constants.TradeConstants;
import com.jsmile.cloud.tradecenter.api.enums.OrderStatusEnum;
import com.jsmile.cloud.tradecenter.api.to.OrderStatusChangeTo;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 订单状态变化(商品扣减库存、添加销量)
 *
 * @author 亮亮
 * @version 1.0.0
 * @date 2017年3月30日 下午3:34:16
 */
@Component
@Slf4j
@AllArgsConstructor
public class OrderStatusChangeListener {

    private final OrderStatusChangeStrategyMap orderStatusChangeStrategyMap;

    @RabbitListener(queuesToDeclare = @Queue(TradeConstants.TradeMq.GOODS_ORDER_STATUS_SYNC_QUEUE))
    public void handler(OrderStatusChangeTo orderStatusChangeTo) {
        log.info("订单状态变化：{}", JSON.toJSONString(orderStatusChangeTo));
        orderStatusChangeStrategyMap.findStrategyOptional(OrderStatusEnum.nameOf(orderStatusChangeTo.getOrderStatus()))
            .ifPresent(iOrderStatusStrategy -> iOrderStatusStrategy.execute(orderStatusChangeTo));
    }
}
