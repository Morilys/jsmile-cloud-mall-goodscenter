package com.jsmile.cloud.goodscenter.spu.repo;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jsmile.cloud.goodscenter.api.enums.GoodsCodeEnum;
import com.jsmile.cloud.goodscenter.spu.dao.SkuInfoDao;
import com.jsmile.cloud.goodscenter.spu.model.SkuInfo;
import com.jsmile.cloud.goodscenter.spu.model.SkuSpec;
import com.jsmile.mall.api.enums.SfEnum;
import com.jsmile.mall.api.exception.ExceptionKit;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2020年06月04日 03:56:49
 */
@Service
@Slf4j
@Transactional(readOnly = true, rollbackFor = Exception.class)
@AllArgsConstructor
public class SkuInfoSearchRepository extends ServiceImpl<SkuInfoDao, SkuInfo> {

    private final SkuSpecSearchRepository skuSpecSearchRepository;

    public List<SkuInfo> findBySkuIdInAndTenantId(List<Long> skuIds, Long tenantId) {
        return this.baseMapper
            .selectList(new QueryWrapper<SkuInfo>().lambda().in(SkuInfo::getSkuId, skuIds).eq(SkuInfo::getTenantId, tenantId).eq(SkuInfo::getDelFlag, SfEnum.F.getValue()));
    }

    public SkuInfo findBySkuIdAndTenantId(Long skuId, Long tenantId) {
        SkuInfo skuInfo = this.baseMapper
            .selectOne(new QueryWrapper<SkuInfo>().lambda().eq(SkuInfo::getSkuId, skuId).eq(SkuInfo::getTenantId, tenantId).eq(SkuInfo::getDelFlag, SfEnum.F.getValue()));
        ExceptionKit.checkNotNull(skuInfo, GoodsCodeEnum.STOCK_NOT_ENOUGH.getResCode(), GoodsCodeEnum.STOCK_NOT_ENOUGH.getResMsg());
        List<SkuSpec> skuSpecs = skuSpecSearchRepository.findBySkuIdAndSpuId(skuInfo.getSkuId(), skuInfo.getSpuId());
        skuInfo.setSkuSpecs(skuSpecs);
        return skuInfo;
    }

    public SkuInfo findBySkuIdAndTenantIdAndNumber(Long skuId, Long tenantId, int skuNumber, Boolean hasCheck) {
        SkuInfo skuInfo = this.baseMapper.selectOne(new QueryWrapper<SkuInfo>().lambda().eq(SkuInfo::getSkuId, skuId).eq(SkuInfo::getTenantId, tenantId)
            .ge(hasCheck, SkuInfo::getInventory, skuNumber).eq(SkuInfo::getDelFlag, SfEnum.F.getValue()));
        ExceptionKit.checkNotNull(skuInfo, GoodsCodeEnum.SKU_NOT_EXISTS.getResCode(), GoodsCodeEnum.SKU_NOT_EXISTS.getResMsg());
        List<SkuSpec> skuSpecs = skuSpecSearchRepository.findBySkuIdAndSpuId(skuInfo.getSkuId(), skuInfo.getSpuId());
        skuInfo.setSkuSpecs(skuSpecs);
        return skuInfo;
    }
}
