package com.jsmile.cloud.goodscenter.spu.event;

import com.jsmile.cloud.goodscenter.spu.model.Spu;

import lombok.Getter;

/**
 * 商品终止发布事件
 */
@Getter
public class SpuEndPublishvent extends SpuEvent {

    public SpuEndPublishvent(Spu spu) {
        super(spu);
    }
}
