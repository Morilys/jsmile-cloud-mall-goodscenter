package com.jsmile.cloud.goodscenter.cate.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2020年06月04日 04:18:34
 */
@ApiModel
@Data
@TableName("t_brand")
public class Brand extends Model<Brand> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @ApiModelProperty(value = "租户id")
    private Long tenantId;
    @ApiModelProperty(value = "分类id")
    private Long categoryId;
    @ApiModelProperty(value = "品牌英文名")
    private String nameEn;
    @ApiModelProperty(value = "品牌中文名")
    private String nameCh;
    @ApiModelProperty(value = "品牌详细介绍")
    private String detail;
    @ApiModelProperty(value = "品牌图")
    private String imageUrl;
    @ApiModelProperty(value = "删除标记位 0:有效 1:删除")
    @TableLogic
    private Integer delFlag;
    @ApiModelProperty(value = "排序")
    private Integer sort;
    @ApiModelProperty(value = "create_time")
    private java.util.Date createTime;
    @ApiModelProperty(value = "update_time")
    private java.util.Date updateTime;
    @ApiModelProperty(value = "create_by")
    private String createBy;
    @ApiModelProperty(value = "update_by")
    private String updateBy;
    @ApiModelProperty(value = "平台id")
    private Integer platformId;
    @ApiModelProperty(value = "业务id")
    private Integer bizCode;
    @ApiModelProperty(value = "备注")
    private String remark;

}
