package com.jsmile.cloud.goodscenter.spu.event;

import com.jsmile.cloud.goodscenter.spu.model.Spu;

import lombok.Getter;

/**
 * 商品上架事件
 */
@Getter
public class SpuPutEvent extends SpuEvent {

    public SpuPutEvent(Spu spu) {
        super(spu);
    }
}
