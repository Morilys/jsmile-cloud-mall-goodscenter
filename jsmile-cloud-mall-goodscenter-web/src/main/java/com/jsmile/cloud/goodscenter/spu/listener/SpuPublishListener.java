// package com.jsmile.cloud.goodscenter.spu.listener;
//
// import cn.hutool.core.util.ObjectUtil;
// import com.google.common.collect.Lists;
// import com.jsmile.api.JSmileResult;
// import com.jsmile.api.to.JsmilePage;
// import com.jsmile.cloud.accountcenter.api.feign.ShopServiceApi;
// import com.jsmile.cloud.accountcenter.api.feign.TenantServiceApi;
// import com.jsmile.cloud.accountcenter.api.req.shop.ReqTenantQuery;
// import com.jsmile.cloud.accountcenter.api.res.ResTenant;
// import com.jsmile.cloud.goodscenter.spu.event.SpuPublishEvent;
// import com.jsmile.cloud.goodscenter.spu.model.SkuInfo;
// import com.jsmile.cloud.goodscenter.spu.model.Spu;
// import com.jsmile.cloud.goodscenter.spu.repo.SkuInfoRepository;
// import com.jsmile.cloud.goodscenter.spu.repo.SkuInfoSearchRepository;
// import com.jsmile.cloud.goodscenter.spu.repo.SpuRepository;
// import com.jsmile.cloud.goodscenter.spu.repo.SpuSearchRepository;
// import lombok.extern.slf4j.Slf4j;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.context.event.EventListener;
// import org.springframework.core.annotation.Order;
// import org.springframework.scheduling.annotation.Async;
// import org.springframework.stereotype.Component;
// import org.springframework.util.CollectionUtils;
//
// import java.util.List;
//
/// **
// * 商品发布后同步到所有商户
// */
// @Slf4j
// @Component
// public class SpuPublishListener {
//
// @Autowired
// private TenantServiceApi tenantServiceApi;
// @Autowired
// private ShopServiceApi shopServiceApi;
// @Autowired
// private SpuRepository spuRepository;
// @Autowired
// private SkuInfoRepository skuInfoRepository;
// @Autowired
// private SpuSearchRepository spuSearchRepository;
// @Autowired
// private SkuInfoSearchRepository skuInfoSearchRepository;
//
// @Async
// @Order
// @EventListener(SpuPublishEvent.class)
// public void spuPublish(SpuPublishEvent event) {
// log.info("spu发布：{}",event.getSpu());
// JSmileResult<JsmilePage<ResTenant>>
// jSmileResult=tenantServiceApi.getTenantPageList(ReqTenantQuery.builder().build());
// List<Spu> spus= Lists.newArrayList();
// List<Spu> updateSpus= Lists.newArrayList();
// List<SkuInfo> skuInfos= Lists.newArrayList();
// List<SkuInfo> updateSkuInfos= Lists.newArrayList();
// if(jSmileResult.getResSuccess()){
// jSmileResult.getResData().getList().stream().filter(x->!x.getId().equals(event.getSpu().getTenantId())).forEach(x->{
// Spu spu = spuSearchRepository.getSimpleSpu(event.getSpu().getSpuId(), x.getId());
// if(null==spu){
// spu= ObjectUtil.clone(event.getSpu());
// spu.setId(null);
// spu.setTenantId(x.getId());
// spu.setSpuDetail(event.getSpu().getSpuDetail());
// spu.setLowPlatformPrice(event.getSpu().getLowPlatformPrice());
// spu.setHighPlatformPrice(event.getSpu().getHighPlatformPrice());
// spus.add(spu);
// }else{
// spu.setSpuDetail(event.getSpu().getSpuDetail());
// spu.setLowPlatformPrice(event.getSpu().getLowPlatformPrice());
// spu.setHighPlatformPrice(event.getSpu().getHighPlatformPrice());
// updateSpus.add(spu);
// }
//
// log.info("租户id:{}",x.getId());
// event.getSpu().getSkuList().forEach(y->{
// SkuInfo skuInfo = skuInfoSearchRepository.findBySkuIdAndTenantId(y.getSkuId(), x.getId());
// if(null==skuInfo){
// skuInfo= ObjectUtil.clone(y);;
// skuInfo.setId(null);
// skuInfo.setTenantId(x.getId());
// skuInfo.setPlatformPrice(y.getPlatformPrice());
// skuInfos.add(skuInfo);
// }else{
// skuInfo.setPlatformPrice(y.getPlatformPrice());
// updateSkuInfos.add(skuInfo);
// }
// });
// });
//
// if(!CollectionUtils.isEmpty(spus)){
// spuRepository.saveBatch(spus);
// }
// if(!CollectionUtils.isEmpty(updateSpus)){
// spuRepository.updateBatchById(updateSpus);
// }
// if(!CollectionUtils.isEmpty(skuInfos)){
// skuInfoRepository.saveBatch(skuInfos);
// }
// if(!CollectionUtils.isEmpty(updateSkuInfos)){
// skuInfoRepository.updateBatchById(updateSkuInfos);
// }
// }
// }
//
//
// }
