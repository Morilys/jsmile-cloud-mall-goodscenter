package com.jsmile.cloud.goodscenter.spu.model;

import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.jsmile.cloud.goodscenter.api.constants.GoodsConstants;
import com.jsmile.cloud.goodscenter.api.req.spu.ReqSkuAdd;
import com.jsmile.cloud.goodscenter.api.req.spu.ReqSkuEdit;
import com.jsmile.cloud.goodscenter.spu.event.SkuDeleteEvent;
import com.jsmile.cloud.goodscenter.spu.repo.SkuInfoSearchRepository;
import com.jsmile.mail.security.event.DomainEventPublisher;
import com.jsmile.mall.api.enums.SfEnum;
import com.jsmile.mall.cache.kit.SpringKit;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.IdUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2020年06月04日 03:56:49
 */
@ApiModel
@Data
@TableName("t_sku_info")
public class SkuInfo extends Model<SkuInfo> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @ApiModelProperty(value = "skuid")
    private Long skuId;
    @ApiModelProperty(value = "第三方skuid")
    private String thirdSkuId;
    @ApiModelProperty(value = "商品编码")
    private Long spuId;
    @ApiModelProperty(value = "条形码")
    private String skuCode;
    @ApiModelProperty(value = "规格货号")
    private String extendCode;
    @ApiModelProperty(value = "成本价 单位:分")
    private Long costPrice;
    @ApiModelProperty(value = "平台价")
    private Long platformPrice;
    @ApiModelProperty(value = "销售价 单位:分")
    private Long sellingPrice;
    @ApiModelProperty(value = "券后价 单位:分")
    private Long couponPrice;
    @ApiModelProperty(value = "市场价，单位：分")
    private Long marketPrice;
    @ApiModelProperty(value = "活动价、团购价 单位:分")
    private Long activityPrice;
    @ApiModelProperty(value = "可售库存")
    private Integer inventory;
    @ApiModelProperty(value = "sku图片地址")
    private String imageUrl;
    @ApiModelProperty(value = "删除标记位 0:有效 1:删除")
    @TableLogic
    private Integer delFlag;
    @ApiModelProperty(value = "update_time")
    private Date updateTime;
    @ApiModelProperty(value = "create_time")
    private Date createTime;
    @ApiModelProperty(value = "重量, 单位:克")
    private Integer weight;
    @ApiModelProperty(value = "租户id")
    private Long tenantId;
    @ApiModelProperty(value = "平台id")
    private Integer platformId;
    @ApiModelProperty(value = "业务id")
    private Integer bizCode;

    @TableField(exist = false)
    @ApiModelProperty(value = "规格信息")
    private List<SkuSpec> skuSpecs;

    public static List<SkuInfo> create(List<ReqSkuAdd> skuList, Long tenantId, Long spuId) {
        AtomicInteger atomicInteger = new AtomicInteger(0);
        return skuList.stream().map(x -> {
            SkuInfo skuInfo = BeanUtil.toBean(x, SkuInfo.class);
            skuInfo.setSkuId(IdUtil.getSnowflake(GoodsConstants.SNOWFLAKE.workerId, atomicInteger.incrementAndGet()).nextId());
            skuInfo.setCreateTime(new Date());
            skuInfo.setTenantId(tenantId);
            skuInfo.setSpuId(spuId);
            skuInfo.setSkuSpecs(SkuSpec.create(x.getSpecs(), tenantId, spuId, skuInfo.getSkuId()));
            return skuInfo;
        }).collect(Collectors.toList());
    }

    public static List<SkuInfo> edit(List<ReqSkuEdit> skuList, Long tenantId, Long spuId) {
        return skuList.stream().map(x -> {
            // ExceptionKit.checkNotNull(x.getMarketPrice(),"市场价不能为空");
            // ExceptionKit.checkNotNull(x.getCostPrice(),"成本价不能为空");
            // ExceptionKit.checkArgument(x.getMarketPrice()<x.getCostPrice(), "市场价必须大于等于成本价!");
            SkuInfo skuInfo = BeanUtil.toBean(x, SkuInfo.class);;
            if (null == x.getSkuId()) {
                skuInfo.setSkuId(IdUtil.getSnowflake(GoodsConstants.SNOWFLAKE.workerId, GoodsConstants.SNOWFLAKE.datacenterId).nextId());
                skuInfo.setCreateTime(new Date());
            } else {
                skuInfo = SpringKit.getBean(SkuInfoSearchRepository.class).getById(x.getSkuId());
                skuInfo.setSkuId(x.getSkuId());
                skuInfo.setUpdateTime(new Date());
                skuInfo.setExtendCode(x.getExtendCode());
                skuInfo.setInventory(x.getInventory());
                skuInfo.setSkuCode(x.getSkuCode());
                skuInfo.setMarketPrice(x.getMarketPrice());
                skuInfo.setWeight(x.getWeight());
                skuInfo.setCostPrice(x.getCostPrice());
                skuInfo.setImageUrl(x.getImageUrl());

            }
            skuInfo.setTenantId(tenantId);
            skuInfo.setSpuId(spuId);
            skuInfo.setSkuSpecs(SkuSpec.create(x.getSpecs(), tenantId, spuId, skuInfo.getSkuId()));
            return skuInfo;
        }).collect(Collectors.toList());
    }

    public static List<SkuInfo> delete(List<ReqSkuEdit> skuList, Long tenantId, Long spuId) {
        return skuList.stream().map(x -> {
            SkuInfo skuInfo = BeanUtil.toBean(x, SkuInfo.class);;
            skuInfo.setTenantId(tenantId);
            skuInfo.setSpuId(spuId);
            skuInfo.setDelFlag(SfEnum.S.getValue());
            DomainEventPublisher.publish(new SkuDeleteEvent(skuInfo));
            return skuInfo;
        }).collect(Collectors.toList());
    }
}
