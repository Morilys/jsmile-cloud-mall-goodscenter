package com.jsmile.cloud.goodscenter.spu.repo;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jsmile.cloud.goodscenter.spu.dao.SpuDao;
import com.jsmile.cloud.goodscenter.spu.model.Spu;
import com.jsmile.mall.api.enums.JSmileCacheEnum;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2020年06月04日 03:56:49
 */
@Service
@Slf4j
@Transactional(readOnly = false, rollbackFor = Exception.class)
@AllArgsConstructor
public class SpuRepository extends ServiceImpl<SpuDao, Spu> {

    private final SkuInfoRepository skuInfoRepository;
    private final SkuSpecRepository skuSpecDao;
    private final SpuImageRepository spuImageRepository;

    /**
     * 商品新增（用于供应商或者店铺商品新增）
     */
    public Boolean saveSpu(Spu spu) {
        skuInfoRepository.saveBatch(spu.getSkuList());
        spuImageRepository.saveBatch(spu.getImageDetailUrls());
        spuImageRepository.saveBatch(spu.getImageUrls());
        spu.getSkuList().forEach(x -> {
            skuSpecDao.saveBatch(x.getSkuSpecs());
        });
        return retBool(this.baseMapper.insert(spu));
    }

    /**
     * 商品修改（用于供应商商品编辑）
     */
    @CacheEvict(value = JSmileCacheEnum.JSmileCacheName.oneMonth, key = "'spu_'+#spu.spuId+'tenantId'+#spu.tenantId")
    public Boolean editSpu(Spu spu) {
        if (!CollectionUtils.isEmpty(spu.getSkuList())) {
            if (skuInfoRepository.deleteBySpuIdAndTenantId(spu.getSpuId(), spu.getTenantId())) {
                skuInfoRepository.saveBatch(spu.getSkuList());
                // skuInfoRepository.saveBatch(spu.getDeleteSkuList());//保存历史删除数据
            } ;
            if (skuSpecDao.deleteBySpuId(spu.getSpuId())) {
                spu.getSkuList().forEach(x -> {
                    skuSpecDao.saveBatch(x.getSkuSpecs());
                });
            } ;
        }
        if (!CollectionUtils.isEmpty(spu.getImageUrls()) && !CollectionUtils.isEmpty(spu.getImageDetailUrls())) {
            if (spuImageRepository.deleteBySpuId(spu.getSpuId())) {
                spuImageRepository.saveBatch(spu.getImageDetailUrls());
                spuImageRepository.saveBatch(spu.getImageUrls());
            } ;
        }
        return retBool(this.baseMapper.update(spu, new UpdateWrapper<Spu>().lambda().eq(Spu::getSpuId, spu.getSpuId()).eq(Spu::getTenantId, spu.getTenantId())));
    }

    /**
     * 商品sku修改（用于商品发布和审核通过）
     */
    @CacheEvict(value = JSmileCacheEnum.JSmileCacheName.oneMonth, key = "'spu_'+#spu.spuId+'tenantId'+#spu.tenantId")
    public Boolean editSpuBySku(Spu spu) {
        if (!CollectionUtils.isEmpty(spu.getSkuList())) {
            skuInfoRepository.updateBatchById(spu.getSkuList());
        }
        return retBool(this.baseMapper.updateById(spu));
    }

    /**
     * 商品库存扣减
     */
    public Boolean reduceStock(Long spuId, Long skuId, int skuNumber) {
        skuInfoRepository.getBaseMapper().reduceStock(spuId, skuId, skuNumber);
        return retBool(this.baseMapper.reduceStock(spuId, skuNumber));
    }

    /**
     * 商品库存回滚
     */
    public Boolean addStock(Long spuId, Long skuId, int skuNumber) {
        skuInfoRepository.getBaseMapper().addStock(spuId, skuId, skuNumber);
        return retBool(this.baseMapper.addStock(spuId, skuNumber));
    }
}
