package com.jsmile.cloud.goodscenter.spu.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jsmile.cloud.goodscenter.spu.model.SkuSpec;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2020年06月04日 03:56:49
 */
public interface SkuSpecDao extends BaseMapper<SkuSpec> {

    @Select("select * from t_sku_spec where sku_id=#{skuId} AND spu_id=#{spuId} ")
    public List<SkuSpec> findBySkuIdAndSpuId(@Param("skuId") Long skuId, @Param("spuId") Long spuId);

    @Select("select * from t_sku_spec where  spu_id=#{spuId} ")
    public List<SkuSpec> findBySpuId(@Param("spuId") Long spuId);

}
