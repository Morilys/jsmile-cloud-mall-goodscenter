package com.jsmile.cloud.goodscenter.cate.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jsmile.cloud.goodscenter.cate.model.FreightTemplate;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2020年06月04日 04:18:34
 */
public interface FreightTemplateDao extends BaseMapper<FreightTemplate> {

}
