package com.jsmile.cloud.goodscenter.cate.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2020年06月04日 04:18:34
 */
@ApiModel
@Data
@TableName("t_category")
public class Category extends Model<Category> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "类目id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @ApiModelProperty(value = "父类id")
    private Long parentId;
    @ApiModelProperty(value = "类目名")
    private String categoryName;
    @ApiModelProperty(value = "排序")
    private Integer sort;
    @ApiModelProperty(value = "层级")
    private Integer level;
    @ApiModelProperty(value = "是否叶子节点 1是 0否")
    private Integer hasLeaf;
    @ApiModelProperty(value = "删除标记位 0:有效 1:删除")
    private Integer delFlag;
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
    @ApiModelProperty(value = "创建者")
    private String createBy;
    @ApiModelProperty(value = "更新者")
    private String updateBy;
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
    @ApiModelProperty(value = "分类描述")
    private String remark;
    @ApiModelProperty(value = "商户Id")
    private Long tenantId;
    @ApiModelProperty(value = "平台id")
    private Integer platformId;
    @ApiModelProperty(value = "业务id")
    private Integer bizCode;

}
