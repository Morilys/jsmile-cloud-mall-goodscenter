package com.jsmile.cloud.goodscenter.spu.repo;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jsmile.cloud.goodscenter.spu.dao.ShopSpuDao;
import com.jsmile.cloud.goodscenter.spu.model.ShopSpu;

import lombok.extern.slf4j.Slf4j;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2020年06月04日 03:56:49
 */
@Service
@Slf4j
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class ShopSpuSearchRepository extends ServiceImpl<ShopSpuDao, ShopSpu> {

    public ShopSpu getSimpleShopSpu(long spuId, long shopId, long tenantId) {
        ShopSpu spu = this.baseMapper.selectOne(
            new QueryWrapper<ShopSpu>().lambda().eq(ShopSpu::getTenantId, tenantId).eq(ShopSpu::getShopId, shopId).eq(ShopSpu::getSpuId, spuId).eq(ShopSpu::getTenantId, tenantId));
        return spu;
    }
}
