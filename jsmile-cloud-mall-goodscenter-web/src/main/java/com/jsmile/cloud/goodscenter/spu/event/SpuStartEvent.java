package com.jsmile.cloud.goodscenter.spu.event;

import com.jsmile.cloud.goodscenter.spu.model.Spu;

import lombok.Getter;

/**
 * 商品启售事件
 */
@Getter
public class SpuStartEvent extends SpuEvent {

    public SpuStartEvent(Spu spu) {
        super(spu);
    }
}
