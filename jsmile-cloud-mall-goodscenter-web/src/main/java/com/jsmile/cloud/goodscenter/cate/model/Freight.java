package com.jsmile.cloud.goodscenter.cate.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2020年06月04日 04:18:34
 */
@ApiModel
@Data
@TableName("t_freight")
public class Freight extends Model<Freight> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @ApiModelProperty(value = "模板名")
    private String name;
    @ApiModelProperty(value = "配送方式：1快递 ")
    private Integer type;
    @ApiModelProperty(value = "是否系统模板： 0：否   1：是")
    private Integer hasSys;
    @ApiModelProperty(value = "是否包邮：  0：不包邮；1：包邮")
    private Integer hasPost;
    @ApiModelProperty(value = "del_flag")
    private Integer delFlag;
    @ApiModelProperty(value = "create_time")
    private java.util.Date createTime;
    @ApiModelProperty(value = "update_time")
    private java.util.Date updateTime;
    @ApiModelProperty(value = "创建者")
    private String createBy;
    @ApiModelProperty(value = "修改者")
    private String updateBy;
    @ApiModelProperty(value = "租户id")
    private Long tenantId;
    @ApiModelProperty(value = "平台id")
    private Integer platformId;
    @ApiModelProperty(value = "业务id")
    private Integer bizCode;

}
