package com.jsmile.cloud.goodscenter.spu.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jsmile.cloud.goodscenter.spu.model.SpuCollect;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2020年06月04日 03:56:49
 */
public interface SpuCollectDao extends BaseMapper<SpuCollect> {

}
