package com.jsmile.cloud.goodscenter.spu.event;

import com.jsmile.cloud.goodscenter.spu.model.Spu;

import lombok.Getter;

/**
 * 商品下架事件
 */
@Getter
public class SpuPullEvent extends SpuEvent {

    public SpuPullEvent(Spu spu) {
        super(spu);
    }
}
