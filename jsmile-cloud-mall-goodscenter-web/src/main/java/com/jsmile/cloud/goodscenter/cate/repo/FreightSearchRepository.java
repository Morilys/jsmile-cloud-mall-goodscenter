package com.jsmile.cloud.goodscenter.cate.repo;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jsmile.cloud.goodscenter.cate.dao.FreightDao;
import com.jsmile.cloud.goodscenter.cate.model.Freight;

import lombok.extern.slf4j.Slf4j;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2020年06月04日 04:18:34
 */
@Service
@Slf4j
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class FreightSearchRepository extends ServiceImpl<FreightDao, Freight> {

}
