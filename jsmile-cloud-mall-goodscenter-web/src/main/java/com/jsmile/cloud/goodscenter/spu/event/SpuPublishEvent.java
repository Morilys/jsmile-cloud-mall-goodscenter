package com.jsmile.cloud.goodscenter.spu.event;

import com.jsmile.cloud.goodscenter.spu.model.Spu;

import lombok.Getter;

/**
 * 商品发布事件
 */
@Getter
public class SpuPublishEvent extends SpuEvent {

    public SpuPublishEvent(Spu spu) {
        super(spu);
    }
}
