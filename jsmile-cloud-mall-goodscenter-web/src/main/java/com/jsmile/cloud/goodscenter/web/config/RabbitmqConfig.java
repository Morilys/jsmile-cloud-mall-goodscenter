package com.jsmile.cloud.goodscenter.web.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.jsmile.cloud.tradecenter.api.constants.TradeConstants;

import lombok.extern.slf4j.Slf4j;

/**
 * rabbitmq消息配置
 *
 * @author 亮亮
 * @version 1.0.0
 * @date 2017年3月30日 下午3:34:16
 */
@Configuration
@Slf4j
public class RabbitmqConfig {

    @Bean
    public Queue orderDirectQueue() {
        return new Queue(TradeConstants.TradeMq.GOODS_ORDER_STATUS_SYNC_QUEUE, true); // true 是否持久
    }

    @Bean
    DirectExchange orderDirectExchange() {
        return new DirectExchange(TradeConstants.TradeMq.GOODS_ORDER_STATUS_SYNC_EXCHANGE);
    }

    @Bean
    Binding bindingOrderDirect() {
        return BindingBuilder.bind(orderDirectQueue()).to(orderDirectExchange()).with(TradeConstants.TradeMq.GOODS_ORDER_STATUS_SYNC_ROUTE);
    }

}
