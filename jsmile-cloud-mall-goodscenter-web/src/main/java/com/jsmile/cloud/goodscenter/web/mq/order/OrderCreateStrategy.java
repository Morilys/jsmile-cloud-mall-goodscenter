package com.jsmile.cloud.goodscenter.web.mq.order;

import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.jsmile.cloud.goodscenter.spu.model.RepertoryRecord;
import com.jsmile.cloud.goodscenter.spu.service.RepertoryRecordApplicationService;
import com.jsmile.cloud.tradecenter.api.enums.OrderStatusEnum;
import com.jsmile.cloud.tradecenter.api.to.OrderStatusChangeTo;

import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 订单创建策略
 *
 * @author gl
 */
@Slf4j
@Component
public class OrderCreateStrategy implements IOrderStatusStrategy {

    @Resource
    private RepertoryRecordApplicationService repertoryRecordApplicationService;

    @Override
    public void execute(OrderStatusChangeTo orderStatusChangeTo) {
        log.info("订单创建事件：{}", JSONUtil.toJsonStr(orderStatusChangeTo));
        // 扣减库存
        reduceStock(orderStatusChangeTo);
    }

    /**
     * 扣减库存
     *
     * @param orderStatusChangeTo
     */
    private void reduceStock(OrderStatusChangeTo orderStatusChangeTo) {
        repertoryRecordApplicationService.create(orderStatusChangeTo.getOrder().getOrderProducts().stream().map(x -> {
            return RepertoryRecord.builder().tenantId(orderStatusChangeTo.getOrder().getTenantId()).orderNo(orderStatusChangeTo.getOrder().getOrderNo() + "")
                .shopId(orderStatusChangeTo.getOrder().getShopId()).skuId(x.getSkuId()).activityId(orderStatusChangeTo.getOrder().getActivityId()).spuId(x.getSpuId())
                .num(x.getProductNumber()).status(OrderStatusEnum.TO_PAY.getValue() == orderStatusChangeTo.getOrderStatus() ? 0 : 1).build();
        }).collect(Collectors.toList()));
    }

    @Override
    public OrderStatusEnum orderStatus() {
        return OrderStatusEnum.TO_PAY;
    }
}
