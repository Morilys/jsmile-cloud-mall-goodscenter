package com.jsmile.cloud.goodscenter.cate.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2020年06月04日 04:18:34
 */
@ApiModel
@Data
@TableName("t_front_category")
public class FrontCategory extends Model<FrontCategory> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "前端分类id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @ApiModelProperty(value = "该类目有父类则是父类id，没有则为0表示该类是根节点")
    private Long parentId;
    @ApiModelProperty(value = "前端分类名")
    private String frontCateName;
    @ApiModelProperty(value = "排序")
    private Integer sort;
    @ApiModelProperty(value = "分类图")
    private String frontCateImg;
    @ApiModelProperty(value = "分类状态：1展示，0隐藏")
    private Integer hasShow;
    @ApiModelProperty(value = "分类层级")
    private Integer level;
    @ApiModelProperty(value = "是否叶子节点 1是 0否")
    private Integer hasLeaf;
    @ApiModelProperty(value = "租户id")
    private Long tenantId;
    @ApiModelProperty(value = "del_flag")
    private Integer delFlag;
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
    @ApiModelProperty(value = "创建者")
    private String createBy;
    @ApiModelProperty(value = "更新者")
    private java.util.Date updateBy;
    @ApiModelProperty(value = "平台id")
    private Integer platformId;
    @ApiModelProperty(value = "业务id")
    private Integer bizCode;
    @ApiModelProperty(value = "备注")
    private String remark;
}
