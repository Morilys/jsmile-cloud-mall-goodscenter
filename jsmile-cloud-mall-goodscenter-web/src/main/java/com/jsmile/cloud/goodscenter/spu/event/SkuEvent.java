package com.jsmile.cloud.goodscenter.spu.event;

import com.jsmile.cloud.goodscenter.spu.model.SkuInfo;
import com.jsmile.mail.security.event.DomainEvent;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SkuEvent extends DomainEvent {

    private final SkuInfo skuInfo;
}
