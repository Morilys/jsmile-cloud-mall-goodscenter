package com.jsmile.cloud.goodscenter.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 商品来源(枚举)
 *
 * @author Morily
 */
@Getter
@AllArgsConstructor
public enum SaleStatusEnum {

    PUT(1, "上架"), PULL(2, "下架"), UNKONW(0, "未知"),;

    /**
     * 值
     */
    private int value;
    /**
     * 名称
     */
    private String name;

}
