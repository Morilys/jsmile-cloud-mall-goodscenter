package com.jsmile.cloud.goodscenter.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 商品来源(枚举)
 *
 * @author Morily
 */
@Getter
@AllArgsConstructor
public enum PublishStatusEnum {

    PUBLISH(1, "发布"), NOT_PUBLISH(2, "未发布"), END_PUBLISH(3, "终止发布"), UNKONW(0, "未知"),;

    /**
     * 值
     */
    private int value;
    /**
     * 名称
     */
    private String name;

}
