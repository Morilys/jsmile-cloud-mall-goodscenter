package com.jsmile.cloud.goodscenter.api.req.spu;

import com.jsmile.mall.api.to.BasePageQueryTo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2019年04月01日 04:45:35
 */
@Data
@ApiModel
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ReqSpuQuery extends BasePageQueryTo {

    @ApiModelProperty(value = "供应商id")
    private Long supplyId;

    @ApiModelProperty(value = "商品编号")
    private String spuNo;
    @ApiModelProperty(value = "商品名称")
    private String spuName;
    @ApiModelProperty(value = "供应商名称")
    private String supplyName;

    @ApiModelProperty(value = "后端类目id")
    private Long categoryId;
    @ApiModelProperty(value = "品牌id")
    private Long brandId;
    @ApiModelProperty(value = "上架状态 1上架 2下架")
    private Integer saleStatus;
    @ApiModelProperty(value = "审核状态 1：未提交，2:待审核，3:未通过，4:通过")
    private Integer reviewStatus;
    @ApiModelProperty(value = "1:启售，2:停售")
    private Integer saleFlag;
    @ApiModelProperty(value = "发布状态 1:发布，2:未发布")
    private Integer publishStatus;
    @ApiModelProperty(value = "商品是否显示 1：是 0：否")
    private Integer hasShow;
}
