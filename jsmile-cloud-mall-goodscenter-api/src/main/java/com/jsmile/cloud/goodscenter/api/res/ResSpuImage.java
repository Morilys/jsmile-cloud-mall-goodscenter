package com.jsmile.cloud.goodscenter.api.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel
@Data
public class ResSpuImage {

    @ApiModelProperty(value = "spu_id")
    private Long spuId;
    @ApiModelProperty(value = "图片url")
    private String imageUrl;
    @ApiModelProperty(value = "租户id")
    private Long tenantId;
}
