package com.jsmile.cloud.goodscenter.api.enums;

public enum GoodsCodeEnum {

    SPU_NOT_EXISTS(130001, "SPU不存在!"), SKU_NOT_EXISTS(130002, "SKU不存在!"), STOCK_NOT_ENOUGH(130003, "商品已售罄!"), GOODS_DOWN(130004, "商品已下架!"), GOODS_NOT_SALE(130005, "商品已停售!"),
    GOODS_NOT_PUBLISH(130006, "商品未发布!"), GOODS_OUT_RANGE(130007, "该地区已无货"), GOODS_INADEQUATE(13008, "商品库存不足"),;

    /**
     * 错误信息
     */
    private String resMsg;
    /**
     * 错误码
     */
    private Integer resCode;

    GoodsCodeEnum(int resCode, String resMsg) {
        this.resCode = resCode;
        this.resMsg = resMsg;
    }

    public String getResMsg() {
        return resMsg;
    }

    public void setResMsg(String resMsg) {
        this.resMsg = resMsg;
    }

    public Integer getResCode() {
        return resCode;
    }

    public void setResCode(Integer resCode) {
        this.resCode = resCode;
    }
}
