package com.jsmile.cloud.goodscenter.api.req.spu;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.jsmile.mall.api.to.BaseUpdateTo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 商品审核
 * @date 2019年04月01日 04:45:35
 */
@Data
@ApiModel
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ReqSpuReviewNot extends BaseUpdateTo {

    @ApiModelProperty("商品id")
    @NotNull(message = "商品id不能为空")
    private Long spuId;
    @ApiModelProperty(value = "审核不通过原因")
    @NotBlank(message = "审核不通过原因不能为空")
    private String reviewErrMsg;

}
