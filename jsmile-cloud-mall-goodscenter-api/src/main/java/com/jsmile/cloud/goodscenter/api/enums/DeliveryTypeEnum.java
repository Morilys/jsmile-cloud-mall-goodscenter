package com.jsmile.cloud.goodscenter.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 配送方式 类型(枚举)
 *
 * @author Morily
 */
@Getter
@AllArgsConstructor
public enum DeliveryTypeEnum {

    SELF(1, "自提"), EXPRESS(2, "快递配送"), UNKONW(0, "未知"),;

    /**
     * 值
     */
    private int value;
    /**
     * 名称
     */
    private String name;

}
