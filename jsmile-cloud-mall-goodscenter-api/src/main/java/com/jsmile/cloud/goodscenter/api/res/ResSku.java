package com.jsmile.cloud.goodscenter.api.res;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

import com.google.common.base.Joiner;
import com.jsmile.cloud.goodscenter.api.enums.PublishStatusEnum;
import com.jsmile.cloud.goodscenter.api.enums.SaleFlagEnum;
import com.jsmile.cloud.goodscenter.api.enums.SaleStatusEnum;

import cn.hutool.core.util.ObjectUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel
@Data
public class ResSku {
    @ApiModelProperty(value = "skuid")
    private Long skuId;
    @ApiModelProperty(value = "第三方skuid")
    private String thirdSkuId;
    @ApiModelProperty(value = "商品编码")
    private Long spuId;
    @ApiModelProperty(value = "条形码")
    private String skuCode;
    @ApiModelProperty(value = "规格货号")
    private String extendCode;
    @ApiModelProperty(value = "成本价 单位:分")
    private Long costPrice;
    @ApiModelProperty(value = "平台价")
    private Long platformPrice;
    @ApiModelProperty(value = "销售价 单位:分")
    private Long sellingPrice;
    @ApiModelProperty(value = "券后价 单位:分")
    private Long couponPrice;
    @ApiModelProperty(value = "市场价，单位：分")
    private Long marketPrice;
    @ApiModelProperty(value = "活动价、团购价 单位:分")
    private Long activityPrice;
    @ApiModelProperty(value = "可售库存")
    private Integer inventory;
    @ApiModelProperty(value = "sku图片地址")
    private String imageUrl;
    @ApiModelProperty(value = "update_time")
    private java.util.Date updateTime;
    @ApiModelProperty(value = "create_time")
    private java.util.Date createTime;
    @ApiModelProperty(value = "重量, 单位:克")
    private Integer weight;
    @ApiModelProperty(value = "租户id")
    private Long tenantId;

    @ApiModelProperty(value = "规格信息")
    private List<ResSkuSpec> skuSpecs;
    @ApiModelProperty(value = "商品信息")
    private SpuInfo spuInfo;
    @ApiModelProperty(value = "specid+skuspecid用于规格定位sku")
    private String skuSpecIdValue;

    public String getSkuSpecIdValue() {
        if (CollectionUtils.isEmpty(this.getSkuSpecs()))
            return null;
        return Joiner.on("_").join(this.getSkuSpecs().stream().map(ResSkuSpec::getSpecValueAndSpecId).collect(Collectors.toList()));
    }

    // 是否有效商品
    public Boolean getHasGoodsSale() {
        if (null == this.getSpuInfo()) {
            return false;
        }
        if (!ObjectUtil.equal(SaleFlagEnum.OPEN.getValue(), this.getSpuInfo().getSaleFlag())) {
            return false;
        }
        if (!ObjectUtil.equal(SaleStatusEnum.PUT.getValue(), this.getSpuInfo().getSaleStatus())) {
            return false;
        }
        if (!ObjectUtil.equal(SaleStatusEnum.PUT.getValue(), this.getSpuInfo().getShopSaleStatus())) {
            return false;
        }
        if (!ObjectUtil.equal(PublishStatusEnum.PUBLISH.getValue(), this.getSpuInfo().getPublishStatus())) {
            return false;
        }
        if (this.getInventory() <= 0) {// 库存不足
            return false;
        }
        return true;
    }

    @Data
    @ApiModel
    public static class SpuInfo {
        @ApiModelProperty(value = "商品id")
        private Long spuId;
        @ApiModelProperty(value = "第三方商品id")
        private String thirdSpuId;
        @ApiModelProperty(value = "供应商id")
        private Long supplyId;
        @ApiModelProperty(value = "商品名称")
        private String spuName;
        @ApiModelProperty(value = "供应商名称")
        private String supplyName;
        @ApiModelProperty(value = "详情描述")
        private String spuDetail;
        @ApiModelProperty(value = "备注")
        private String remark;
        @ApiModelProperty(value = "后端类目id")
        private Long categoryId;
        @ApiModelProperty(value = "品牌id")
        private Long brandId;
        @ApiModelProperty(value = "商品编号")
        private String spuNo;
        @ApiModelProperty(value = "缩略图")
        private String imageUrl;
        @ApiModelProperty(value = "副标题")
        private String subHead;
        @ApiModelProperty(value = "最低起买: 单位为lot_size")
        private Integer minBuyNum;
        @ApiModelProperty(value = "重量, 单位:克")
        private Integer weight;
        @ApiModelProperty(value = "包装单位")
        private String pkgUnit;
        @ApiModelProperty(value = "单位")
        private String unit;
        @ApiModelProperty(value = "二维码图")
        private String qrcodeUrl;
        @ApiModelProperty(value = "配送方式,1:自提,2:快递配送")
        private Integer deliveryType;
        @ApiModelProperty(value = "运费模板id")
        private Long templateId;
        @ApiModelProperty(value = "上架状态 1上架 2下架")
        private Integer saleStatus;
        @ApiModelProperty(value = "店铺上架状态 1上架 2下架")
        private Integer shopSaleStatus = 1;
        @ApiModelProperty(value = "审核状态 1：未提交，2:待审核，3:未通过，4:通过")
        private Integer reviewStatus;
        @ApiModelProperty(value = "1:启售，2:停售")
        private Integer saleFlag;
        @ApiModelProperty(value = "发布状态 1:发布，2:未发布")
        private Integer publishStatus;
        @ApiModelProperty(value = "最低销售价")
        private Long lowSellingPrice;
        @ApiModelProperty(value = "最高销售价")
        private Long highSellingPrice;
        @ApiModelProperty(value = "最低市场价")
        private Long lowMarketPrice;
        @ApiModelProperty(value = "最高市场价")
        private Long highMarketPrice;
        @ApiModelProperty(value = "最低券后价")
        private Long lowCouponPrice;
        @ApiModelProperty(value = "最高券后价")
        private Long highCouponPrice;
        @ApiModelProperty(value = "最低平台价")
        private Long lowPlatformPrice;
        @ApiModelProperty(value = "最高平台价")
        private Long highPlatformPrice;
    }
}
