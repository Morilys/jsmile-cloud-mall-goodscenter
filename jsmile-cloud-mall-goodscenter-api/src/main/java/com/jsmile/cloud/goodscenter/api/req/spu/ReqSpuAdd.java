package com.jsmile.cloud.goodscenter.api.req.spu;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.jsmile.mall.api.to.BaseUpdateTo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2019年04月01日 04:45:35
 */
@Data
@ApiModel
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ReqSpuAdd extends BaseUpdateTo {

    @ApiModelProperty("后端分类id")
    @NotNull(message = "分类id不能为空")
    private Long categoryId;
    @ApiModelProperty("品牌id")
    private Long brandId;
    @ApiModelProperty("供应商id")
    private Long supplyId;
    @ApiModelProperty("店铺id")
    private Long shopId;
    @ApiModelProperty(value = "商品名称")
    private String spuName;
    @ApiModelProperty(value = "详情描述")
    private String spuDetail;
    @NotBlank(message = "商品编码不能为空")
    private String spuNo;
    @ApiModelProperty(value = "商品主图")
    @Size(min = 1, max = 10)
    @NotNull(message = "商品主图不能为空")
    private List<String> imageUrl;
    @ApiModelProperty(value = "商品详情图")
    @Size(min = 1, max = 10)
    @NotNull(message = "商品主图不能为空")
    private List<String> imageDetailUrl;
    @ApiModelProperty(value = "商品详情图")
    @Size(min = 1, max = 100)
    @NotNull(message = "商品sku不能为空")
    private List<ReqSkuAdd> skuList;

    @ApiModelProperty(value = "商品类型 1:供应商商品 2:店铺商品")
    private Integer spuType;
}
