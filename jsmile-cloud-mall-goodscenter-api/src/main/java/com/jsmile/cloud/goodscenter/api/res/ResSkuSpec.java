package com.jsmile.cloud.goodscenter.api.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel
@Data
public class ResSkuSpec {

    @ApiModelProperty(value = "id")
    private Long id;
    @ApiModelProperty(value = "sku_id")
    private Long skuId;
    @ApiModelProperty(value = "spu_id")
    private Long spuId;
    @ApiModelProperty(value = "规格id")
    private Long specId;
    @ApiModelProperty(value = "规格名")
    private String specName;
    @ApiModelProperty(value = "规格值")
    private String specValue;
    @ApiModelProperty(value = "租户id")
    private Long tenantId;

    public String getSpecValueAndSpecId() {
        return this.getSpecValue() + "_" + this.getSpecId();
    }
}
