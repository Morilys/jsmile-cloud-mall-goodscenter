package com.jsmile.cloud.goodscenter.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 商城是否显示商品
 *
 * @author Morily
 */
@Getter
@AllArgsConstructor
public enum ShareFlagEnum {

    SHARE(1, "分享"), NOT_SHARE(0, "不分享"),;

    /**
     * 值
     */
    private int value;
    /**
     * 名称
     */
    private String name;

}
