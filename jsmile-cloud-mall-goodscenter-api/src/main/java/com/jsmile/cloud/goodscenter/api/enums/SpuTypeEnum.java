package com.jsmile.cloud.goodscenter.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 商品来源(枚举)
 *
 * @author Morily
 */
@Getter
@AllArgsConstructor
public enum SpuTypeEnum {

    SUPPLY(1, "供应商商品"), SHOP(2, "店铺商品"), UNKONW(0, "未知"),;

    /**
     * 值
     */
    private int value;
    /**
     * 名称
     */
    private String name;

}
