package com.jsmile.cloud.goodscenter.api.req.spu;

import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.jsmile.mall.api.to.BaseTo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2019年04月01日 04:45:35
 */
@Data
@ApiModel
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReqSkuCheck extends BaseTo {

    @ApiModelProperty(value = "是否开启校验:默认不开启(购物车详情不开启)，下单需要开启")
    public Boolean hasCheck = false;
    @ApiModelProperty(value = "商品列表")
    @Size(min = 1, max = 1000)
    @NotNull(message = "商品sku不能为空")
    private List<SkuCheck> skuList;

    @Data
    @ApiModel
    public static class SkuCheck {
        @ApiModelProperty("skuid")
        @NotNull(message = "skuid不能为空")
        private Long skuId;
        @ApiModelProperty("spuId")
        @NotNull(message = "spuId不能为空")
        private Long spuId;
        @ApiModelProperty(value = "供应商id")
        private Long supplyId;
        @ApiModelProperty(value = "店铺id")
        private Long shopId;
        @ApiModelProperty(value = "商品数量")
        @NotNull
        @Min(1)
        @Max(100)
        private Integer skuNumber;
    }
}
