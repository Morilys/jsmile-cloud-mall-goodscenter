package com.jsmile.cloud.goodscenter.api.req.spu;

import com.jsmile.mall.api.to.BasePageQueryTo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2019年04月01日 04:45:35
 */
@Data
@ApiModel
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ReqSpuDetail extends BasePageQueryTo {

    @ApiModelProperty(value = "spuid")
    private Long spuId;
    @ApiModelProperty(value = "店铺id")
    private Long shopId;
    @ApiModelProperty(value = "供应商id")
    private Long supplyId;
}
