package com.jsmile.cloud.goodscenter.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 商品来源(枚举)
 *
 * @author Morily
 */
@Getter
@AllArgsConstructor
public enum GoodsReviewStatusEnum {

    NOT_SUBMIT(1, "未提交"), CHECK(2, "待审核"), NOT_PASS(3, "未通过"), PASS(4, "通过"), UNKONW(0, "未知"),;

    /**
     * 值
     */
    private int value;
    /**
     * 名称
     */
    private String name;

}
