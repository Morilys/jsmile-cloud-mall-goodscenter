package com.jsmile.cloud.goodscenter.api.res;

import java.util.List;

import com.jsmile.cloud.goodscenter.api.enums.PublishStatusEnum;
import com.jsmile.cloud.goodscenter.api.enums.SaleFlagEnum;
import com.jsmile.cloud.goodscenter.api.enums.SaleStatusEnum;

import cn.hutool.core.util.ObjectUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class ResSpu {

    @ApiModelProperty(value = "商品id")
    private Long spuId;
    @ApiModelProperty(value = "第三方商品id")
    private String thirdSpuId;
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
    @ApiModelProperty(value = "创建人")
    private String createBy;
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
    @ApiModelProperty(value = "更新人")
    private String updateBy;
    @ApiModelProperty(value = "租户id")
    private Long tenantId;
    @ApiModelProperty(value = "供应商id")
    private Long supplyId;
    @ApiModelProperty(value = "商品名称")
    private String spuName;
    @ApiModelProperty(value = "供应商名称")
    private String supplyName;
    @ApiModelProperty(value = "详情描述")
    private String spuDetail;
    @ApiModelProperty(value = "备注")
    private String remark;
    @ApiModelProperty(value = "后端类目id")
    private Long categoryId;
    @ApiModelProperty(value = "品牌id")
    private Long brandId;
    @ApiModelProperty(value = "商品编号")
    private String spuNo;
    @ApiModelProperty(value = "缩略图")
    private String imageUrl;
    @ApiModelProperty(value = "副标题")
    private String subHead;
    @ApiModelProperty(value = "最低起买: 单位为lot_size")
    private Integer minBuyNum;
    @ApiModelProperty(value = "重量, 单位:克")
    private Integer weight;
    @ApiModelProperty(value = "包装单位")
    private String pkgUnit;
    @ApiModelProperty(value = "单位")
    private String unit;
    @ApiModelProperty(value = "二维码图")
    private String qrcodeUrl;
    @ApiModelProperty(value = "配送方式,1:自提,2:快递配送")
    private Integer deliveryType;
    @ApiModelProperty(value = "商品类型 1:供应商商品 2:店铺商品")
    private Integer spuType;
    @ApiModelProperty(value = "运费模板id")
    private Long templateId;
    @ApiModelProperty(value = "上架状态 1上架 2下架")
    private Integer saleStatus;
    @ApiModelProperty(value = "审核状态 1：未提交，2:待审核，3:未通过，4:通过")
    private Integer reviewStatus;
    @ApiModelProperty(value = "1:启售，2:停售")
    private Integer saleFlag;
    @ApiModelProperty(value = "发布状态 1:发布，2:未发布")
    private Integer publishStatus;
    @ApiModelProperty(value = "店铺上架状态 1上架 2下架")
    private Integer shopSaleStatus = 1;
    @ApiModelProperty(value = "最低销售价")
    private Long lowSellingPrice;
    @ApiModelProperty(value = "最高销售价")
    private Long highSellingPrice;
    @ApiModelProperty(value = "最低市场价")
    private Long lowMarketPrice;
    @ApiModelProperty(value = "最高市场价")
    private Long highMarketPrice;
    @ApiModelProperty(value = "最低券后价")
    private Long lowCouponPrice;
    @ApiModelProperty(value = "最高券后价")
    private Long highCouponPrice;
    @ApiModelProperty(value = "最低平台价")
    private Long lowPlatformPrice;
    @ApiModelProperty(value = "最高平台价")
    private Long highPlatformPrice;
    @ApiModelProperty(value = "商品销量")
    private Integer goodsSales;
    @ApiModelProperty(value = "商品库存")
    private Integer goodsStock;
    @ApiModelProperty(value = "审核不通过原因")
    private String reviewErrMsg;

    @ApiModelProperty(value = "sku信息")
    private List<ResSku> skuList;
    @ApiModelProperty(value = "商品主图")
    private List<ResSpuImage> imageUrls;
    @ApiModelProperty(value = "商品详情图")
    private List<ResSpuImage> imageDetailUrls;
    @ApiModelProperty(value = "sku规格信息")
    private List<ResSpec> specs;

    // 是否有效商品
    public Boolean getHasGoodsSale() {
        if (!ObjectUtil.equal(SaleFlagEnum.OPEN.getValue(), this.getSaleFlag())) {
            return false;
        }
        if (!ObjectUtil.equal(SaleStatusEnum.PUT.getValue(), this.getSaleStatus())) {
            return false;
        }
        if (!ObjectUtil.equal(SaleStatusEnum.PUT.getValue(), this.getShopSaleStatus())) {
            return false;
        }
        if (!ObjectUtil.equal(PublishStatusEnum.PUBLISH.getValue(), this.getPublishStatus())) {
            return false;
        }
        if (this.getGoodsStock() <= 0) {// 库存不足
            return false;
        }
        return true;
    }
}
