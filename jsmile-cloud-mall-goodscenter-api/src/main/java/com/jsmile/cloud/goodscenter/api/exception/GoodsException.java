package com.jsmile.cloud.goodscenter.api.exception;

import com.jsmile.cloud.goodscenter.api.enums.GoodsCodeEnum;
import com.jsmile.mall.api.enums.JSmileCodeEnum;
import com.jsmile.mall.api.exception.BaseException;

public class GoodsException extends BaseException {

    public GoodsException(Integer errCode, String errMsg, Throwable e) {
        super(errCode, errMsg, e);
    }

    public GoodsException(GoodsCodeEnum k, Throwable e) {
        super(k.getResCode(), k.getResMsg(), e);
    }

    public GoodsException(GoodsCodeEnum k) {
        super(k.getResCode(), k.getResMsg(), null);
    }

    public GoodsException(JSmileCodeEnum k) {
        super(k.getResCode(), k.getResMsg(), (Throwable)null);
    }

    public GoodsException(String errMsg, Throwable e) {
        super(errMsg, e);
    }

    public GoodsException(String errMsg) {
        super(errMsg, null);
    }
}
