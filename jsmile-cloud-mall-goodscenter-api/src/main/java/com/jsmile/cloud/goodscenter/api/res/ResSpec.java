package com.jsmile.cloud.goodscenter.api.res;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel
@Data
public class ResSpec {

    @ApiModelProperty(value = "规格名")
    private String specName;
    @ApiModelProperty(value = "id")
    private Long id;
    @ApiModelProperty(value = "规格组合")
    private List<ResSpecGroup> specGroups;
    @ApiModelProperty(value = "租户id")
    private Long tenantId;

    @ApiModel
    @Data
    public static class ResSpecGroup {
        @ApiModelProperty(value = "规格值")
        private String specValue;
        @ApiModelProperty(value = "规格名")
        private String specName;
        @ApiModelProperty(value = "spu_id")
        private Long spuId;
        @ApiModelProperty(value = "规格id")
        private Long specId;
    }
}
