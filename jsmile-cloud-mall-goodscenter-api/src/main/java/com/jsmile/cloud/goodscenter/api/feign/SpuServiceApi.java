package com.jsmile.cloud.goodscenter.api.feign;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jsmile.cloud.goodscenter.api.constants.GoodsConstants;
import com.jsmile.cloud.goodscenter.api.req.spu.*;
import com.jsmile.cloud.goodscenter.api.res.ResSpu;
import com.jsmile.mall.api.JSmileResult;
import com.jsmile.mall.api.to.JsmilePage;

@FeignClient(value = GoodsConstants.APPLICATION_NAME, contextId = "spu")
public interface SpuServiceApi {

    // TODO 新增商品
    @RequestMapping(value = "/feign/spu/addSpu", method = RequestMethod.POST)
    public JSmileResult register(@RequestBody @Valid ReqSpuAdd reqSpuAdd);

    // TODO 编辑商品
    @RequestMapping(value = "/feign/spu/editSpu", method = RequestMethod.POST)
    public JSmileResult editSpu(@RequestBody @Valid ReqSpuEdit reqSpuEdit);

    // TODO 商品上架
    @RequestMapping(value = "/feign/spu/putSpuByMerchant", method = RequestMethod.POST)
    public JSmileResult putSpuByMerchant(@RequestBody @Valid ReqSpuSaleStatus reqSpuSaleStatus);

    // TODO 商品下架
    @RequestMapping(value = "/feign/spu/pullSpuByMerchant", method = RequestMethod.POST)
    public JSmileResult pullSpuByMerchant(@RequestBody @Valid ReqSpuDelete reqSpuDelete);

    // TODO 商品审核通过并发布
    @RequestMapping(value = "/feign/spu/reviewSpu", method = RequestMethod.POST)
    public JSmileResult reviewSpu(@RequestBody @Valid ReqSpuReview reqSpuReview);

    // TODO 商品审核不通过
    @RequestMapping(value = "/feign/spu/reviewSpuNot", method = RequestMethod.POST)
    public JSmileResult reviewSpuNot(@RequestBody @Valid ReqSpuReviewNot reqSpuReviewNot);

    // TODO 供应商、运营、商户(小程序平台)商品查询
    @RequestMapping(value = "/feign/spu/getSpuPageList", method = RequestMethod.POST)
    public JSmileResult<JsmilePage<ResSpu>> getSpuPageList(@RequestBody @Valid ReqSpuQuery reqSpuQuery);

    // TODO 复杂商品查询（包含sku等信息）
    @RequestMapping(value = "/feign/spu/getSpu", method = RequestMethod.POST)
    public JSmileResult getSpu(@RequestBody @Valid ReqSpuDetail reqSpuDetail);

    // TODO 简单商品查询
    @RequestMapping(value = "/feign/spu/getSimpleSpu", method = RequestMethod.POST)
    public JSmileResult getSimpleSpu(@RequestBody @Valid ReqSpuDetail reqSpuDetail);
}
