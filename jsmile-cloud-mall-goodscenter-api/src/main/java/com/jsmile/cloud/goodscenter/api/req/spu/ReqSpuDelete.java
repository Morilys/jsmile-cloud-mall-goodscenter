package com.jsmile.cloud.goodscenter.api.req.spu;

import javax.validation.constraints.NotNull;

import com.jsmile.mall.api.to.BaseUpdateTo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2019年04月01日 04:45:35
 */
@Data
@ApiModel
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ReqSpuDelete extends BaseUpdateTo {

    @ApiModelProperty("商品id")
    @NotNull(message = "spuId不能为空")
    private Long spuId;

}
