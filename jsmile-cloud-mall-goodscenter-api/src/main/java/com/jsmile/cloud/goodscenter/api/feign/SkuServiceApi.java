package com.jsmile.cloud.goodscenter.api.feign;

import java.util.List;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jsmile.cloud.goodscenter.api.constants.GoodsConstants;
import com.jsmile.cloud.goodscenter.api.req.spu.ReqSkuCheck;
import com.jsmile.cloud.goodscenter.api.res.ResSku;
import com.jsmile.mall.api.JSmileResult;

@FeignClient(value = GoodsConstants.APPLICATION_NAME, contextId = "sku")
public interface SkuServiceApi {

    // TODO 商品库存和状态检查
    @RequestMapping(value = "/feign/sku/skuCheck", method = RequestMethod.POST)
    JSmileResult<List<ResSku>> skuCheck(@RequestBody @Valid ReqSkuCheck reqSkuCheck);
}
