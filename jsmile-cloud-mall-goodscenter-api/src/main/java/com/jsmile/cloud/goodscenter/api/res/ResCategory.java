package com.jsmile.cloud.goodscenter.api.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class ResCategory {

    @ApiModelProperty(value = "商品id")
    private Long spuId;
    @ApiModelProperty(value = "父类id")
    private Long parentId;
    @ApiModelProperty(value = "类目名")
    private String categoryName;
    @ApiModelProperty(value = "租户id")
    private Long tenantId;
}
