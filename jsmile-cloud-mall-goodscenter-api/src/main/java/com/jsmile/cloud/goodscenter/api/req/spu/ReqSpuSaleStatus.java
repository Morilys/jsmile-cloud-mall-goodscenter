package com.jsmile.cloud.goodscenter.api.req.spu;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.jsmile.mall.api.to.BaseUpdateTo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 商品上架
 * @date 2019年04月01日 04:45:35
 */
@Data
@ApiModel
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ReqSpuSaleStatus extends BaseUpdateTo {

    @ApiModelProperty("商品id")
    @NotNull(message = "商品id不能为空")
    private Long spuId;

    @ApiModelProperty(value = "商品列表")
    @Size(min = 1, max = 1000)
    @NotNull(message = "商品sku不能为空")
    private List<SkuSaleStatus> skuList;

    @Data
    @ApiModel
    public static class SkuSaleStatus {
        @ApiModelProperty("skuid")
        @NotNull(message = "skuid不能为空")
        private Long skuId;

        @ApiModelProperty(value = "销售价 单位:分")
        @NotNull(message = "销售价不能为空")
        private Long sellingPrice;
        @ApiModelProperty(value = "券后价 单位:分")
        @NotNull(message = "券后价不能为空")
        private Long couponPrice;
    }
}
