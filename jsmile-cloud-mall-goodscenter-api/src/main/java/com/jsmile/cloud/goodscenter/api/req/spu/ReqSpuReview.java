package com.jsmile.cloud.goodscenter.api.req.spu;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.jsmile.mall.api.to.BaseUpdateTo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 商品审核
 * @date 2019年04月01日 04:45:35
 */
@Data
@ApiModel
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ReqSpuReview extends BaseUpdateTo {

    @ApiModelProperty("商品id")
    @NotNull(message = "商品id不能为空")
    private Long spuId;
    @ApiModelProperty(value = "详情描述")
    private String spuDetail;

    @ApiModelProperty(value = "商品列表")
    @Size(min = 1, max = 1000)
    @NotNull(message = "商品sku不能为空")
    private List<SkuReview> skuList;

    @Data
    @ApiModel
    public static class SkuReview {
        @ApiModelProperty("skuid")
        @NotNull(message = "skuid不能为空")
        private Long skuId;

        @ApiModelProperty(value = "平台价")
        @NotNull(message = "平台价不能为空")
        private Long platformPrice;
    }
}
