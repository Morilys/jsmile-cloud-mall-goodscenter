package com.jsmile.cloud.goodscenter.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 商品启停售(枚举)
 *
 * @author Morily
 */
@Getter
@AllArgsConstructor
public enum SaleFlagEnum {

    OPEN(1, "启售"), CLOSE(2, "停售"), UNKONW(0, "未知"),;

    /**
     * 值
     */
    private int value;
    /**
     * 名称
     */
    private String name;

}
