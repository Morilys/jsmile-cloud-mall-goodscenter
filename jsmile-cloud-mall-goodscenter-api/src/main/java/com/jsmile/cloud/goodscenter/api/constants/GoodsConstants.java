package com.jsmile.cloud.goodscenter.api.constants;

/**
 * 权限常量
 *
 * @author 龚亮
 */
public interface GoodsConstants extends java.io.Serializable {

    /**
     * 服务名
     */
    String APPLICATION_NAME = "goodscenter";

    public static class SNOWFLAKE {
        public static final long workerId = 1L;
        public static final long datacenterId = 4L;
    }
}
