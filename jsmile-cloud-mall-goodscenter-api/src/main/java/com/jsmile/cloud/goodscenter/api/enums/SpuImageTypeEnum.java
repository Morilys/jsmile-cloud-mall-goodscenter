package com.jsmile.cloud.goodscenter.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 商城是否显示商品
 *
 * @author Morily
 */
@Getter
@AllArgsConstructor
public enum SpuImageTypeEnum {

    HEAD(1, "头图"), DETAIL(2, "详情图"),;

    private int value;
    private String name;

}
