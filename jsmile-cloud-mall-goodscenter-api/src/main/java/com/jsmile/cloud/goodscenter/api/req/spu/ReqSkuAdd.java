package com.jsmile.cloud.goodscenter.api.req.spu;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.jsmile.mall.api.to.BaseUpdateTo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author 龚亮
 * @version 1.0
 * @desc 为了薪资破万，还是睡一会儿吧...!
 * @date 2019年04月01日 04:45:35
 */
@Data
@ApiModel
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ReqSkuAdd extends BaseUpdateTo {

    @ApiModelProperty(value = "第三方skuid")
    private String thirdSkuId;
    @ApiModelProperty(value = "条形码")
    private String skuCode;
    @ApiModelProperty(value = "规格货号")
    private String extendCode;
    @ApiModelProperty(value = "规格说明 逗号隔开 例: 11:颜色:红,12:尺寸:大号")
    @Size(min = 1, max = 100)
    @NotNull(message = "规格不能为空")
    private List<ReqSpecAdd> specs;
    @ApiModelProperty(value = "成本价 单位:分")
    @NotNull(message = "成本价不能为空")
    private Long costPrice;
    @ApiModelProperty(value = "市场价，单位：分")
    @NotNull(message = "市场价不能为空")
    private Long marketPrice;
    @ApiModelProperty(value = "可售库存")
    @NotNull(message = "库存不能为空")
    private Integer inventory;
    @ApiModelProperty(value = "sku图片地址")
    private String imageUrl;
    @ApiModelProperty(value = "重量, 单位:克")
    private Integer weight;

}
