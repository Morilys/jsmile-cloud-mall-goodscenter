package com.jsmile.cloud.goodscenter.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 商城是否显示商品
 *
 * @author Morily
 */
@Getter
@AllArgsConstructor
public enum HasShowEnum {

    SHOW(1, "显示"), HIDE(0, "隐藏"),;

    /**
     * 值
     */
    private int value;
    /**
     * 名称
     */
    private String name;

}
